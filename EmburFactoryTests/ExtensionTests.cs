using BusinessLogic.Factories;
using EmburFactory.Extensions;
using EmburFactory.Models.MediaDtos;

namespace EmburFactoryTests
{
    public class ExtensionTests
    {
        [Fact]
        public void GivenAModel_CanTransformToDto()
        {
            //Arrange
            var factory = MediaFactory.GetMediaService("book");
            var bookModel = factory.GetMediaItem();

            //Act
            var dto = (BookDto)bookModel.ToDto<BookDto>();

            //Assert
            Assert.Equal("The Hobbit", dto.Title);
            Assert.Equal("J.R.R. Tolkien", dto.Author);
            Assert.Equal(310, dto.Pages);
        }
    }
}