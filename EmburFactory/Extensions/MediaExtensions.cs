﻿using BusinessLogic.Models.Media;
using EmburFactory.Models.MediaDtos;

namespace EmburFactory.Extensions
{
    public static class MediaExtensions
    {
        public static IMediaDto ToDto<T>(this IMediaModel media) where T: IMediaDto
        {
            return media switch
            {
                Book book => new BookDto
                {
                    Title = book.Title,
                    Author = book.Author,
                    Pages = book.Pages
                },
                AudioBook audioBook => new AudioBookDto
                {
                    Title = audioBook.Title,
                    Author = audioBook.Author,
                    Narrator = audioBook.Narrator,
                    LengthInMinutes = audioBook.LengthInMinutes
                },
                _ => throw new NotImplementedException()
            };
        }

        public static IMediaModel ToModel(this IMediaDto media)
        {
            return media switch
            {
                Book bookDto => new Book
                {
                    Title = bookDto.Title,
                    Author = bookDto.Author,
                    Pages = bookDto.Pages
                },
                AudioBook audioBookDto => new AudioBook
                {
                    Title = audioBookDto.Title,
                    Author = audioBookDto.Author,
                    Narrator = audioBookDto.Narrator,
                    LengthInMinutes = audioBookDto.LengthInMinutes
                },
                _ => throw new NotImplementedException()
            };
        }
    }
}
