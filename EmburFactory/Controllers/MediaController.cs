﻿using EmburFactory.Models.MediaDtos;
using EmburFactory.Models.Shared;
using EmburFactory.Services;
using Microsoft.AspNetCore.Mvc;

namespace EmburFactory.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MediaController : ControllerBase
    {
        [HttpGet]
        [Route("GetBook")]
        public ActionResult<ResponseDto<BookDto>> GetBook()
        {
            var response = MediaService.GetMediaItem<BookDto>("book");
            return response.Success ? Ok(response) : BadRequest(response);
        }

        [HttpGet]
        [Route("GetAudioBook")]
        public ActionResult<ResponseDto<AudioBookDto>> GetAudioBook()
        {
            var response = MediaService.GetMediaItem<AudioBookDto>("audiobook");
            return response.Success ? Ok(response) : BadRequest(response);
        }
    }
}
