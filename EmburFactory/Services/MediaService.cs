﻿using BusinessLogic.Factories;
using EmburFactory.Extensions;
using EmburFactory.Models.MediaDtos;
using EmburFactory.Models.Shared;

namespace EmburFactory.Services
{
    public class MediaService
    {
        public static ResponseDto<T> GetMediaItem<T>(string mediaType) where T:IMediaDto
        {
            var response = new ResponseDto<T>
            {
                Success = true
            };

            try
            {
                var mediaService = MediaFactory.GetMediaService(mediaType);
                var mediaItem = mediaService.GetMediaItem();
                response.Data = (T?)mediaItem.ToDto<T>();
            }
            catch (Exception e)
            {
                response.Success = false;
                response.ErrorMessage = e.Message;
            }

            return response;
        }
    }
}
