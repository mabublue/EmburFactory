﻿namespace EmburFactory.Models.MediaDtos
{
    public interface IMediaDto
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}
