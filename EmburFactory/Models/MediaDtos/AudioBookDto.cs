﻿namespace EmburFactory.Models.MediaDtos
{
    public class AudioBookDto : IMediaDto
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public string Narrator { get; set; }
        public int LengthInMinutes { get; set; }
    }
}
