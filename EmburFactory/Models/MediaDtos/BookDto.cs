﻿namespace EmburFactory.Models.MediaDtos
{
    public class BookDto : IMediaDto
    {
        public string Title { get ; set; }
        public string Author { get; set; }
        public int Pages { get; set; }
    }
}
