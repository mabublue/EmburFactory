﻿namespace EmburFactory.Models.Shared
{
    public class ResponseDto<T>
    {
        public bool Success { get; set; }
        public T? Data { get; set; }
        public string? ErrorMessage { get; set; }
    }
}
