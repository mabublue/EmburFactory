﻿namespace BusinessLogic.Models.Media
{
    public class AudioBook : IMediaModel
    {
        public string Title { get; set; }
        public string Author { get; set ; }
        public string Narrator { get; set; }
        public int LengthInMinutes { get; set; }
    }
}
