﻿namespace BusinessLogic.Models.Media
{
    public interface IMediaModel
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }
}