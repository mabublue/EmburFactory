﻿namespace BusinessLogic.Models.Media
{
    public class Book : IMediaModel
    {
        public string Title { get; set; }
        public string Author { get; set; }
        public int Pages { get; set; }
    }
}
