﻿using BusinessLogic.Services;

namespace BusinessLogic.Factories
{
    public static class MediaFactory
    {
        public static IMediaService GetMediaService(string mediaType)
        {
            switch (mediaType.ToLower())
            {
                case "book":
                    return new BookService();
                case "audiobook":
                    return new AudioBookService();
                default:
                    throw new Exception("Invalid media type");
            }
        }
    }
}
