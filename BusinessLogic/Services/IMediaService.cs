﻿using BusinessLogic.Models.Media;

namespace BusinessLogic.Services
{
    public interface IMediaService
    {
        IMediaModel GetMediaItem();
    }
}