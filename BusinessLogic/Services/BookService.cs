﻿using BusinessLogic.Models.Media;

namespace BusinessLogic.Services
{
    public class BookService : IMediaService
    {
        public IMediaModel GetMediaItem()
        {
            return new Book
            {
                Title = "The Hobbit",
                Author = "J.R.R. Tolkien",
                Pages = 310
            };
        }
    }
}
