﻿using BusinessLogic.Models.Media;

namespace BusinessLogic.Services
{
    public class AudioBookService : IMediaService
    {
        public IMediaModel GetMediaItem()
        {
            return new AudioBook
            {
                Title = "The Hobbit",
                Author = "J.R.R. Tolkien",
                LengthInMinutes = 600,
                Narrator = "Rob Inglis"
            };
        }
    }
}
